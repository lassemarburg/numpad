#include <ESP8266WiFi.h> // Import ESP8266 WiFi library
#include <PubSubClient.h>// Import PubSubClient library to initialize MQTT protocol
// Update these with values suitable for your network.
const char* ssid =  "yourSSID"; //use your ssid
const char* password = "yourPassword"; // use your password
const char* mqtt_server = "test.mosquitto.org"; // use your MQTT Broker URL
WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE  (50)
char msg[MSG_BUFFER_SIZE];
int value = 0;

// Die Keypad Library einbinden
#include <Keypad.h>

// Variablen fuer Keypad
const byte ROWS = 4; // Vier Reihen
const byte COLS = 3; // Drei Zeilen
char keys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'#','0','*'}
};
byte rowPins[ROWS] = {D3, D4, D5, D6}; // mit den Reihen Pins verbunden
byte colPins[COLS] = {D0, D1, D2}; // mit den Zeilen Pins verbunden

// Keypad Objekt aus Keypad Library erstellen
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

// String zum speichern der letzten Eingaben
char recent[]= "xxxxxxxxxx";
char current[] = "x";

// Digital Pin Variables for LEDs
int green = D7;
int red = D8;

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  //WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}
// Check for Message received on define topic for MQTT Broker
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  if (strcmp(topic,"devices/numpad/green")==0) {
    if ((char)payload[0] == '1') {
      digitalWrite(green, HIGH);   // Turn the green LED on
    } else {
      digitalWrite(green, LOW);  // Turn the LED off by making the voltage LOW
    }
  } else if (strcmp(topic,"devices/numpad/red")==0) {
    if ((char)payload[0] == '1') {
      digitalWrite(red, HIGH);   // Turn the red LED on
    } else {
      digitalWrite(red, LOW);  // Turn the LED off by making the voltage LOW
    }
  } else if (strcmp(topic,"devices/numpad/clear")==0) {
    for ( byte i = 0; i <10; i++)
    {
      recent[i] = 'x';
    }
  }
}
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str(),"device","6jsFn590OUwv0ZFX0J")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("devices/numpad/connected", "");
      // ... and resubscribe
      client.subscribe("devices/numpad/red");
      client.subscribe("devices/numpad/green");
      client.subscribe("devices/numpad/clear");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
void setup() {
  pinMode(green, OUTPUT);
  pinMode(red, OUTPUT);

  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}
void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  unsigned long now = millis();
  if (now - lastMsg > 5000) {
    lastMsg = now;
    ++value;
    snprintf (msg, MSG_BUFFER_SIZE, "#%ld", value);
    client.publish("devices/numpad/heartbeat", msg);
  }

  char key = keypad.getKey();

  if (key != NO_KEY){
    Serial.print("key: ");
    Serial.println(key);
    current[0] = key;
    client.publish("devices/numpad/key", current);

    memcpy(recent, &recent[1], sizeof(recent) - sizeof(char));
    recent[9] =  key;

    Serial.print("latest keys: ");
    Serial.println(recent);

    client.publish("devices/numpad/latest", recent);
  }
}