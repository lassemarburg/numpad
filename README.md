# Numpad

## Requirements

Keypad Arduino Library

For MQTT Setup:

PuSubClient

## Setup

For the MQTT version replace WiFi credentials in *NumpadMQTT.ino*

```c++
const char* ssid =  "yourSSID"; //use your ssid
const char* password = "yourPassword"; // use your password
```

And if need be the MQTT Broker URL

```c++
const char* mqtt_server = "test.mosquitto.org"; // use your MQTT Broker URL
```

Upload either of the sketches to your board.

## MQTT Topics and Messages

### From Numpad

Every 5 Seconds

`devices/numpad/heartbeat`

On Key Press

`devices/numpad/key X` X is Key Number `0-9` or `#` or `*`

`devices/numpad/latest XXXX` String of all Keys that where pressed since restart or **clear** command.

### To numpad
Set Green LED:

ON:  `devices/numpad/green 1`
OFF: `devices/numpad/green 0`

Set Red LED:

ON:  `devices/numpad/red 1`
OFF: `devices/numpad/red 0`

Clear latest numbers storage:

`devices/numpad/clear`

